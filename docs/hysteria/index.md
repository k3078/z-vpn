## 简介

主项目地址：[HyNetwork/hysteria: Hysteria is a feature-packed network utility optimized for networks of poor quality (e.g. satellite connections, congested public Wi-Fi, connecting from China to servers abroad) (github.com)](https://github.com/HyNetwork/hysteria)

第三方项目地址：[emptysuns/Hi_Hysteria: Hello World！非钟国优化线路使用不佳？不想中转？hysteria一键搞定。 (github.com)](https://github.com/emptysuns/Hi_Hysteria)



Hysteria这是一款由go编写的非常优秀的“轻量”代理程序，它很好的解决了在搭建富强魔法服务器时最大的痛点——线路拉跨。

在魔法咏唱时最难的不是搭建维护，而是在晚高峰时期的交付质量。当三大运营商晚高变成了：奠信、连不通、移不动时，你我都有感触。 虽然是走的udp但是提供obfs，暂时不会被运营商针对性的QoS(不开obfs也不会被QoS)。



请注意部分服务商不支持Hysteria协议/对Hysteria支持不好。可以参考：[Hi_Hysteria/blacklist](https://github.com/emptysuns/Hi_Hysteria/blob/main/md/blacklist.md)

## 快速入门



### 服务端

```
sudo bash <(curl -fsSL https://git.io/hysteria.sh)
```

首次安装后，会自动显示连接信息，包括`config.json `内容和一键订阅链接。

之后如要使用可使用`hihy`命令调出菜单。



### 客户端

### 安卓

推荐使用

### Windows

推荐使用第三方CMD工具，比较方便且有反馈性。

本部分基本转自：[Hi_Hysteria/cmd.md at main · emptysuns/Hi_Hysteria (github.com)](https://github.com/emptysuns/Hi_Hysteria/blob/main/md/cmd.md)。



当出现**安装完毕**字样后，**会自动打印生成的配置信息**，同时**当前目录**下会生成一个config.json文件。

![image](result.png)

可以本地新建一个config.json（**一定要是这个名称！**）文件，复制粘贴到本地**conf**文件夹下，也可以直接下载生成的文件到本地**conf**文件夹下。

将config.json加入[**release**](https://github.com/emptysuns/Hi_Hysteria/releases)中提供的简单的windows cmd客户端的解压目录中.

![image](s1.png)
![image](s2.png)

### 客户端使用

提供两种运行方法：后台运行（无cmd窗口无感） 和 前台运行（必须得有cmd窗口，但是可查看当前日志）

**启动之前**请把config.json放到conf文件夹！

------

- 方法一：后台运行（推荐）

运行:双击back_start.bat

停止:双击back_stop.bat

运行back_start.bat后，可以回车关闭此窗口，不需保留。

[![image](tips.png)](https://raw.githubusercontent.com/emptysuns/Hi_Hysteria/main/imgs/tips.png)

停止后台运行：

[![image](back_stop.png)](https://raw.githubusercontent.com/emptysuns/Hi_Hysteria/main/imgs/back_stop.png)

批处理能力有限，请谅解.

------

- 方法二：前台运行

运行:双击run.bat

停止:回车，或者键入其他非'n'的字符

打开run.bat运行，运行时按回车键停止，防呆键入n继续运行

**直接键入Enter关闭客户端！**

**直接键入Enter关闭客户端！**

**直接键入Enter关闭客户端！**

**切记不要直接关闭cmd窗口！**

**切记不要直接关闭cmd窗口！**

**切记不要直接关闭cmd窗口！**

批处理能力有限，请谅解.

启动

[![image](https://camo.githubusercontent.com/ae8cff1dab104ea4d71783265f1417cdaa6ad37afc99f4ebf33726534242485e/68747470733a2f2f636c6f75642e696163672e63662f303a2f6e6f726d616c2f696d672f686968797374657269612f6d61726b2e706e67)](https://camo.githubusercontent.com/ae8cff1dab104ea4d71783265f1417cdaa6ad37afc99f4ebf33726534242485e/68747470733a2f2f636c6f75642e696163672e63662f303a2f6e6f726d616c2f696d672f686968797374657269612f6d61726b2e706e67)

防呆/关闭

[![image](stop.png)](https://raw.githubusercontent.com/emptysuns/Hi_Hysteria/main/imgs/stop.png)

**Tips:前台运行模式下，不小心关掉窗口导致无法上网时，运行back_stop.bat可以清除代理和关闭hysteria。**

------

### 客户端配置未生效？

如上图启动成功，但代理并未启用，请手动打开设置->网络->代理,查看配置是否生效

[![image](proxy.png)](https://raw.githubusercontent.com/emptysuns/Hi_Hysteria/main/imgs/proxy.png)

### 配置开机自启

~~不会有人不知道吧:)~~

对back_start.bat(**后台模式**) 或者 run.bat(**前台模式**)文件创建一个**快捷方式**

win+r 键入

```
shell:startup
```

打开自启目录将快捷方式**复制**进去，下次开机就会自启动。

[![image](startup.png)](https://raw.githubusercontent.com/emptysuns/Hi_Hysteria/main/imgs/startup.png)

这里用后台做演示，前台同理