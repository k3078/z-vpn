## 使用Heroku部署Vmess/Vless服务

### 优势

- 完全免费。
- 支持一键部署，步骤简单，可靠，速度较快。
- 单账号每月免费额度550小时。
- 注册只需邮箱验证。故理论上可用临时邮箱无限多开。
- 长时间不用（30min）会休眠。
- 私有，无需担心数据被窃取
- 优质，稳定时速度可达10M/s

### 关于heroku

> Heroku是一个支持多种编程语言的云平台即服务。在2010年被Salesforce.com收购。Heroku作为最开始的云平台之一，从2007年6月起开发，当时它仅支持Ruby，但后来增加了对Java、Node.js、Scala、Clojure、Python以及（未记录在正式文件上）PHP和Perl的支持。基础操作系统是Debian，在最新的堆栈则是基于Debian的Ubuntu。

Heroku还提供的免费网站可以用于构建个人网站，非常适用于实验性质或是简单的博客系统（以及搭建梯子）。

## 使用教程

### 部署heroku

首先注册Heroku账号，点击通过[Heroku](https://dashboard.heroku.com/login)  注册一个账号，注册时候不能使用QQ邮箱！

GitHub上有很多使用heroku部署的项目，但是很多都已经被标记。这里放两个我正在使用的：

[kerms5/appppppp (github.com)](https://github.com/kerms5/appppppp)

[kerms5/demogfw: 利用Heroku部署高性能代理免费vless服务 (github.com)](https://github.com/kerms5/demogfw)

点击去，fork一下，然后注意改名！不要包含heroku、v2ray等关键词。转到以下连接

```
# 将template换成你自己的仓库名
https://dashboard.heroku.com/new?template=XXXXXXX
# 例如
https://dashboard.heroku.com/new?template=https://github.com/xd5845/v2ray-heroku1
```

复制里面显示的UUID，然后点击deploy app即可。

>如果你觉得不放心，也可以转到 [此网站](https://www.uuidgenerator.net/) 生成一个UUID贴进去。

部署完成以后，点击 Settings 再点击 Reveal Config Vars 就可以看见 UUID了！记下自己的UUID等会还是用到。

接着下滑，看见Domains项后有个域名！`https://*****.herokuapp.com/` 这就是你的VPN域名了。

### 配置客户端

推荐使用Clash或者V2rayN

使用Clash：

[Releases · Fndroid/clash_for_windows_pkg · GitHub (fastgit.org)](https://hub.fastgit.org/Fndroid/clash_for_windows_pkg/releases)

安装完成之后，修改配置，点击 Profiles，点击 config.yaml，第一个选项。

用记事本打开之后就会看到一堆代码，这就是在修改配置了，我在网上找了一个配置实例，先试一下，要是用不了发邮件告诉我声。

**实例地址：**https://raw.githubusercontent.com/V2RaySSR/Tools/master/clash.yaml

打开之后全部复制，替换之前的内容，修改 “配置开始” 部分。详细内容可以加群 https://t.me/mjjscjl 讨论

### 配置cloudflare（可选）

cloudflare是提供免费的CDN加速服务，套一个cloudflare（据说）可以让网速快一点。反正不套白不套嘛。此外，cloudflare还可以用于ipv6服务器建站。建议注册一个。

**如果不套，则略过这一步，直接跳到第四步，链接就用上面那个。**

首先注册登陆[网络安全ddos防护_免费cdn加速_智能化云服务平台 | Cloudflare 中国官网 | Cloudflare](https://www.cloudflare.com/zh-cn/)，然后点击右侧的Workers ！

接着点击创建Workers

接着复制下方代码，并添加进去！注意把下面的中文替换成你之前在Domains项看见的那个域名前缀。

```
addEventListener(
  "fetch",event => {
     let url=new URL(event.request.url);
     url.hostname="你的heroku域名.herokuapp.com";
     let request=new Request(url,event.request);
     event. respondWith(
       fetch(request)
     )
  }
)
```

保留弹出的项目名下方的连接。替换原heroku连接即可。
