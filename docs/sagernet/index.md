### SagerNet（安卓）

[SagerNet/SagerNet: The universal proxy toolchain for Android (github.com)](https://github.com/SagerNet/SagerNet)

- 安卓平台。
- 支持Vmess、**Hysteria**等协议。支持直接导入。

#### 使用教程（Hysteria）

项目地址：[SagerNet/SagerNet: The universal proxy toolchain for Android (github.com)](https://github.com/SagerNet/SagerNet)

转到：[Download - SagerNet](https://sagernet.org/download/)，需要下载安装 软件本体 和 Hysteria Plugin，两个软件分别安装。

界面右上角从链接导入即可。

- 提示 缺少插件，需要在 设置-软件权限 中**打开 自启动 权限**，其他权限也应当相应开启。否则可能报错。

- 提示`libhysteria exits too fast`，点击服务器右上角编辑图标，打开**允许不安全的链接**。



![image-20220426201950664](image-20220426201950664.png)